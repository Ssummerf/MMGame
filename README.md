# Mega Man Game (C)

This is the end project for CPSC 305. It's a game I designed for the GBA using old Mega Man battle network assets. All of the combat, animations, backgrounds, and sprite work were custom made.

Though the base sprites were all edited, the original artwork was from Mega Man BN 3 and 4.

## Features:

```
Custom made map and collision design
Custom sprite animations for character interaction
Sidescrolling map movement
Enemy AI + Movement
Winning and Losing conditions
```

The overall map was pretty small. It was just to playtest the features I made for the player to interact with. The code can be emulated virtually, or works fine on a regular GBA.

## Main.c

Contains commented methods for the Game Loop / interactions. Essentially all the methods are in here.

## Checkery.s, Framechecker.s

Two assembly functions to be incorporated into our C code. They check certain frames to determine character and ai states.

## Map.h

Our custom map file to run the environment in. There are three iterations to test different coding issues.

## Other Misc .h files (Megaman, Sword, Etc)

Converted frame strips to display our characters and animations using our png converter.

## Ending Remarks

This was an individual assignment, though I did receive guidance and assitance in converting images, as well as other program features, from Ian Finlayson.
