@ framechecker.s

@Simple case return for animation frames

.global framechecker

framechecker:

mov r1, #0

cmp r0, #2
beq enemyone

cmp r0, #1
beq enemytwo

b done

enemyone:
mov r1, #112
b done

enemytwo:
mov r1, #128
b done

done:
mov r0, r1
mov pc, lr

