@ checkery.s

@ Simple equivalency checker for megaman animation checking

.global checkery

@If the number to check is 3, for damage or damage done, return true.
checkery: 

mov r1, #1

cmp r0, #3
beq done

mov r1, #0

done:

mov r0, r1
mov pc, lr

