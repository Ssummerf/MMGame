/* damage.h
 * generated by raw2gba */

#define damage_bytes 2912

const signed char damage [] = {
    0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0x00, 
    0xFF, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x01, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 
    0x01, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xFE, 0xFF, 0x00, 
    0xFD, 0xFE, 0xFF, 0xFE, 0xFE, 0xFE, 0xFD, 0xFD, 0xFE, 0xFC, 0xFF, 0xFD, 
    0xFB, 0xFF, 0xFC, 0xFC, 0xFE, 0xFE, 0xFE, 0xFE, 0xFD, 0xFD, 0x00, 0x00, 
    0xFE, 0x00, 0x01, 0x00, 0x04, 0x01, 0x00, 0x02, 0x05, 0x00, 0x00, 0x05, 
    0x01, 0x02, 0x02, 0x06, 0x04, 0x00, 0x06, 0x05, 0x04, 0x04, 0x03, 0x04, 
    0x05, 0x07, 0x04, 0x01, 0x06, 0x0A, 0x00, 0x01, 0x08, 0x07, 0xFD, 0x03, 
    0x05, 0xFD, 0x06, 0x02, 0x01, 0x00, 0xFA, 0x01, 0x04, 0xFE, 0xFD, 0x07, 
    0xFD, 0xFF, 0x04, 0x03, 0x02, 0x00, 0x05, 0xFF, 0x05, 0x08, 0x00, 0x06, 
    0x09, 0x02, 0x08, 0x06, 0x04, 0x0C, 0x0E, 0x0A, 0x07, 0x08, 0x0C, 0x0D, 
    0x08, 0x08, 0x0D, 0x09, 0x01, 0x09, 0x0A, 0xFC, 0x02, 0x08, 0xFC, 0x01, 
    0x02, 0xF9, 0xFD, 0x00, 0xF6, 0xFC, 0xFB, 0xF3, 0x01, 0x06, 0xFB, 0xE7, 
    0xEA, 0xFB, 0xF4, 0xFE, 0x0E, 0x08, 0x0D, 0x0F, 0x0E, 0x0C, 0xF8, 0x0C, 
    0x19, 0xFF, 0x03, 0xF9, 0x02, 0x04, 0x02, 0xFB, 0xF9, 0x14, 0x01, 0xF7, 
    0x26, 0x12, 0xE7, 0x19, 0x20, 0xF5, 0x11, 0x16, 0xFF, 0x1C, 0x11, 0xF1, 
    0xFE, 0xF3, 0x04, 0x01, 0xD6, 0x01, 0xF0, 0xC2, 0x06, 0xFD, 0xD4, 0xF9, 
    0x0D, 0xE2, 0xE8, 0x0C, 0x07, 0xF2, 0x0D, 0x17, 0x07, 0x1B, 0x0E, 0x1A, 
    0x2E, 0x11, 0x2C, 0x21, 0x11, 0x26, 0x1E, 0x20, 0x27, 0x11, 0x14, 0x26, 
    0x03, 0x08, 0x1F, 0xF9, 0x08, 0xF1, 0xE4, 0x16, 0xD9, 0xC1, 0xF5, 0xE0, 
    0xE7, 0xD6, 0xDB, 0xE0, 0xD3, 0xE6, 0xDC, 0xD8, 0xED, 0xD5, 0xDF, 0xF3, 
    0xE0, 0xEC, 0x04, 0xEE, 0xF1, 0x08, 0xF8, 0x07, 0x09, 0xF4, 0x0E, 0x26, 
    0x0A, 0x0E, 0x2D, 0x18, 0x1C, 0x1F, 0x1C, 0x19, 0x21, 0x28, 0x1D, 0x0F, 
    0x15, 0x16, 0x09, 0x07, 0x03, 0xFE, 0xF2, 0xED, 0xFB, 0xE3, 0xD4, 0xE8, 
    0xEA, 0xC7, 0xC2, 0xF4, 0xD3, 0xC0, 0xE7, 0xF7, 0xC4, 0xCB, 0xF6, 0xCE, 
    0xC9, 0xFC, 0xDA, 0xE1, 0xFC, 0xE3, 0xF1, 0x06, 0xFA, 0x13, 0x03, 0x00, 
    0x27, 0x16, 0x07, 0x28, 0x23, 0x2F, 0x32, 0x09, 0x25, 0x20, 0x2E, 0x2A, 
    0x1E, 0x29, 0x19, 0x22, 0x29, 0x1E, 0x17, 0x1D, 0x10, 0x19, 0x0E, 0xFA, 
    0x09, 0x10, 0xFB, 0xED, 0xEA, 0xE3, 0xE5, 0xE3, 0xD3, 0xC4, 0xD2, 0xDB, 
    0xCF, 0xC5, 0xC9, 0xC3, 0xD4, 0xD6, 0xBE, 0xD4, 0xD7, 0xCF, 0xE5, 0xDC, 
    0xD4, 0xEA, 0xDB, 0xE3, 0x02, 0xDC, 0xF6, 0x0D, 0xE7, 0x0A, 0x06, 0xF9, 
    0x2B, 0x09, 0x03, 0x1F, 0x10, 0x27, 0x22, 0x26, 0x31, 0x27, 0x30, 0x23, 
    0x20, 0x27, 0x2D, 0x2B, 0x19, 0x20, 0x2B, 0x1D, 0x18, 0x20, 0x10, 0x0E, 
    0x23, 0x04, 0x08, 0x1B, 0x03, 0x00, 0x08, 0x00, 0xF9, 0xF3, 0xE5, 0xF2, 
    0xF6, 0xE0, 0xE3, 0xE1, 0xD4, 0xE4, 0xCC, 0xC3, 0xD1, 0xCF, 0xBE, 0xC9, 
    0xC7, 0xB8, 0xD4, 0xD0, 0xBE, 0xCE, 0xD1, 0xCE, 0xDF, 0xD0, 0xD6, 0xE2, 
    0xDD, 0xE8, 0xE9, 0xE8, 0xED, 0xE8, 0xF6, 0x0C, 0xF2, 0xFC, 0x12, 0xED, 
    0x10, 0x1E, 0xF9, 0x1C, 0x21, 0x03, 0x1E, 0x2F, 0x06, 0x22, 0x32, 0x1A, 
    0x27, 0x1C, 0x2D, 0x2A, 0x21, 0x24, 0x21, 0x1F, 0x1D, 0x26, 0x1B, 0x14, 
    0x20, 0x21, 0x0E, 0x11, 0x13, 0x15, 0x04, 0xFD, 0x10, 0xEE, 0x0E, 0x0A, 
    0xE8, 0x08, 0xFC, 0xE8, 0xFA, 0xFD, 0xE3, 0xF4, 0xF8, 0xCD, 0xEB, 0xE6, 
    0xC2, 0xE3, 0xE7, 0xCC, 0xDD, 0xE7, 0xBC, 0xDB, 0xD5, 0xBC, 0xD3, 0xDF, 
    0xC5, 0xCF, 0xDF, 0xD0, 0xE3, 0xEE, 0xCE, 0xDC, 0xE9, 0xD6, 0xE7, 0xEA, 
    0xE7, 0xF4, 0xE4, 0xEE, 0xEC, 0xEE, 0x06, 0xE0, 0x04, 0x1B, 0xE4, 0x18, 
    0x12, 0xFF, 0x10, 0x15, 0x23, 0x16, 0x13, 0x2B, 0x15, 0x18, 0x39, 0x19, 
    0x15, 0x30, 0x1E, 0x1B, 0x35, 0x1D, 0x13, 0x33, 0x15, 0x28, 0x25, 0x0F, 
    0x29, 0x0A, 0x0E, 0x28, 0x1E, 0x08, 0x16, 0x09, 0xFD, 0x19, 0x06, 0xF5, 
    0x0B, 0x0E, 0xF1, 0x14, 0x06, 0xD8, 0x0F, 0x07, 0xD8, 0xF7, 0x08, 0xE4, 
    0xDF, 0xFD, 0xEA, 0xD8, 0xFB, 0xD2, 0xD7, 0xFB, 0xD1, 0xE2, 0xDD, 0xCF, 
    0xFD, 0xD2, 0xB8, 0xEF, 0xD9, 0xC5, 0xD7, 0xDD, 0xE2, 0xCB, 0xD7, 0xE3, 
    0xC4, 0xD0, 0xF3, 0xC9, 0xDA, 0xDF, 0xD1, 0xDE, 0xE9, 0xE6, 0xCF, 0xDE, 
    0x00, 0xFB, 0xD5, 0xF9, 0x07, 0xF2, 0xE6, 0xF8, 0x27, 0x05, 0xE6, 0x02, 
    0x26, 0x18, 0x14, 0x08, 0x1A, 0x2A, 0x04, 0x28, 0x2C, 0xFE, 0x1D, 0x39, 
    0x34, 0x01, 0x23, 0x35, 0x30, 0x16, 0xFF, 0x3E, 0x35, 0x0C, 0x05, 0x34, 
    0x2E, 0xF1, 0x2A, 0x22, 0xF3, 0xF9, 0x2B, 0x32, 0xEF, 0xF8, 0x21, 0x0A, 
    0xF6, 0x15, 0x0F, 0x0A, 0xFC, 0x11, 0xFE, 0xDF, 0x0D, 0x1F, 0xF8, 0xDF, 
    0xED, 0xF9, 0x05, 0x01, 0xD8, 0xD3, 0xF6, 0xE9, 0xED, 0xD2, 0xCF, 0xFE, 
    0xCF, 0xBC, 0xEE, 0xF5, 0xCA, 0xDB, 0xD6, 0xDA, 0xEF, 0xBE, 0xED, 0xF3, 
    0xC1, 0xF0, 0xDE, 0xD8, 0xC9, 0xCE, 0xF3, 0xDA, 0xD5, 0xD1, 0xE4, 0xDA, 
    0xD0, 0xD9, 0xC7, 0xEA, 0xE5, 0xCE, 0xF4, 0xD0, 0xFA, 0x00, 0xE6, 0xF4, 
    0xF0, 0xC6, 0xED, 0x1B, 0xCB, 0x1C, 0x25, 0xCB, 0x0A, 0xF1, 0xF9, 0x28, 
    0xE4, 0xFF, 0x39, 0xC9, 0xF6, 0x2F, 0x19, 0x12, 0xE7, 0x25, 0xEB, 0x02, 
    0x31, 0x12, 0x18, 0xD6, 0x27, 0x30, 0xF6, 0x1C, 0x15, 0x0F, 0x09, 0xFA, 
    0x2D, 0x41, 0xFD, 0x06, 0x2F, 0x28, 0x2F, 0x41, 0xF8, 0x13, 0x0B, 0xF5, 
    0x3F, 0x12, 0x0A, 0x20, 0xF1, 0x28, 0x21, 0xF2, 0x10, 0xF4, 0x0E, 0x00, 
    0xD2, 0x09, 0x2E, 0xC9, 0xE4, 0x0D, 0xE8, 0xE1, 0xDB, 0x18, 0xBE, 0xEB, 
    0x04, 0xD0, 0xD6, 0x01, 0xDA, 0xC1, 0x06, 0xB8, 0xD3, 0xEB, 0xE4, 0xEA, 
    0xCC, 0xE8, 0xBA, 0xF1, 0xDB, 0xE2, 0xF4, 0xEE, 0xD5, 0xBE, 0xFD, 0xD0, 
    0xD8, 0xFD, 0xE9, 0xEF, 0xDE, 0xD9, 0xF0, 0xFD, 0xE2, 0xE6, 0xE3, 0xE6, 
    0xF3, 0xE7, 0xE5, 0xDB, 0xF5, 0xE2, 0xD8, 0x07, 0xE5, 0xF8, 0xE7, 0x15, 
    0x0F, 0xDC, 0xF1, 0xF7, 0xF1, 0xF6, 0x20, 0xE2, 0xEB, 0x1F, 0xD4, 0x13, 
    0x1B, 0xE7, 0x17, 0xEC, 0x08, 0x21, 0xDC, 0x09, 0x01, 0x17, 0x03, 0x09, 
    0xF8, 0xCA, 0x27, 0xFA, 0xD1, 0x05, 0x0B, 0xED, 0x09, 0x04, 0xE3, 0x22, 
    0x07, 0xFF, 0x24, 0x02, 0x15, 0x31, 0xE5, 0x12, 0x2A, 0xF6, 0x40, 0xEF, 
    0x1C, 0x45, 0xD6, 0x47, 0x1A, 0x02, 0x36, 0xEC, 0x3D, 0xFE, 0xF5, 0x38, 
    0xE7, 0x26, 0x1E, 0xDB, 0x08, 0x00, 0xEA, 0xFE, 0x04, 0xD2, 0x01, 0xE3, 
    0xD2, 0x1E, 0xDF, 0xB8, 0x18, 0xEB, 0xD3, 0x07, 0xD7, 0xF6, 0x07, 0xD9, 
    0xEC, 0x08, 0xF8, 0xE1, 0x20, 0xF3, 0xEB, 0x3D, 0xDA, 0xF5, 0x0D, 0xE8, 
    0x18, 0xF9, 0xF3, 0x21, 0x0F, 0xF3, 0x0C, 0x07, 0xEA, 0x14, 0xE2, 0xE8, 
    0xFB, 0xDF, 0x02, 0xD9, 0xF5, 0xFA, 0xDA, 0x08, 0x02, 0xCF, 0xE4, 0x27, 
    0xB8, 0xD3, 0x2C, 0xCA, 0xD3, 0x1C, 0xDD, 0xC5, 0x16, 0xE9, 0xD1, 0x05, 
    0xFE, 0xD3, 0xE4, 0xE9, 0xEE, 0x03, 0xE0, 0xE5, 0x13, 0xE8, 0xE2, 0x06, 
    0x01, 0xF5, 0x00, 0xFD, 0xF3, 0x13, 0x0F, 0xEC, 0xFA, 0x20, 0x11, 0x15, 
    0x11, 0x06, 0x09, 0x00, 0x2B, 0x18, 0xE6, 0x15, 0x28, 0xF5, 0xFC, 0x0F, 
    0xF3, 0x16, 0xFF, 0x0C, 0x10, 0xE3, 0xE9, 0x1B, 0x07, 0xC3, 0x14, 0x1D, 
    0xC3, 0x02, 0x3B, 0xF8, 0xCB, 0x25, 0x1C, 0xB8, 0xE5, 0x3E, 0xEE, 0xE5, 
    0x3F, 0xEE, 0xE5, 0x1E, 0xEA, 0xE3, 0x3F, 0x24, 0xDB, 0x08, 0x10, 0xDC, 
    0xF0, 0xF6, 0x34, 0x1B, 0xD8, 0xE1, 0x1A, 0x22, 0xBC, 0xFB, 0x24, 0xE1, 
    0xEA, 0x13, 0x1B, 0xEB, 0xE5, 0x01, 0x06, 0xD3, 0x10, 0x08, 0xD4, 0x11, 
    0xF8, 0x00, 0x07, 0xCF, 0x03, 0xEF, 0xE9, 0x10, 0xE2, 0x01, 0x12, 0xC6, 
    0xE1, 0x1A, 0xF5, 0xE0, 0xF3, 0xF8, 0xF3, 0xEF, 0x04, 0x07, 0xE1, 0xDC, 
    0x21, 0x06, 0xBD, 0xFC, 0x35, 0xE3, 0xC8, 0x12, 0x1B, 0xD1, 0xEC, 0x26, 
    0xE3, 0xEC, 0xF4, 0x19, 0xF7, 0xD8, 0x0B, 0x04, 0x0A, 0xEE, 0xFD, 0xF9, 
    0xF5, 0xE9, 0x0F, 0x0A, 0xC5, 0xE0, 0x2A, 0xFD, 0xB8, 0x08, 0x11, 0xD8, 
    0xE0, 0x2A, 0x0F, 0xD6, 0xFB, 0x0F, 0xFD, 0xEC, 0x1A, 0x14, 0xED, 0xF2, 
    0x12, 0xEC, 0xF1, 0x12, 0x1E, 0xF4, 0xD8, 0xFC, 0x25, 0xF3, 0xC4, 0x14, 
    0x0B, 0x02, 0xDD, 0xF2, 0x29, 0xF5, 0xD3, 0xF2, 0x0E, 0xEA, 0xEF, 0x0C, 
    0xF1, 0xEE, 0xF7, 0x22, 0x10, 0xDE, 0x1B, 0xF2, 0xF9, 0x0D, 0x07, 0x18, 
    0xD6, 0x03, 0x0B, 0x04, 0xFD, 0xDE, 0x10, 0x0A, 0xD2, 0xE9, 0x18, 0xF9, 
    0xC7, 0xE9, 0x20, 0xFB, 0xE7, 0xE0, 0x08, 0x0D, 0xC7, 0x06, 0x2F, 0xDD, 
    0xD4, 0xDD, 0x3D, 0x22, 0xC8, 0x07, 0x10, 0x08, 0x01, 0x07, 0x18, 0xCC, 
    0x1D, 0x38, 0xEE, 0xF7, 0x10, 0x26, 0x04, 0xF2, 0x19, 0x1E, 0xE2, 0xFB, 
    0x3E, 0xED, 0xD2, 0xFB, 0xF3, 0x3D, 0xFF, 0xC5, 0x08, 0x11, 0x19, 0xC8, 
    0xF5, 0x1D, 0xF8, 0xD6, 0x01, 0x34, 0xE9, 0xE0, 0x09, 0x11, 0xD4, 0xCC, 
    0x0F, 0xE2, 0xEA, 0x0F, 0xFB, 0x01, 0xDA, 0xD5, 0x1A, 0xED, 0xE5, 0x0A, 
    0x11, 0xF2, 0xD6, 0xE5, 0xFC, 0x22, 0xE0, 0xFB, 0x34, 0x0F, 0xD2, 0xFB, 
    0x2F, 0xF2, 0xDA, 0x0E, 0x47, 0xDD, 0xE6, 0x30, 0x15, 0xEC, 0xE4, 0x2F, 
    0x02, 0xFA, 0xE3, 0x05, 0x3C, 0xDC, 0xE5, 0xFD, 0x14, 0xFF, 0xF9, 0xD2, 
    0x00, 0x12, 0xEB, 0x05, 0x09, 0x05, 0xCC, 0xE4, 0x36, 0x09, 0xBF, 0xFE, 
    0x38, 0xE6, 0xEF, 0x07, 0x0F, 0xE0, 0xE6, 0x23, 0xFA, 0xE4, 0xDD, 0x21, 
    0x2A, 0xD0, 0xEB, 0x03, 0xFC, 0xF5, 0xE3, 0x0A, 0x23, 0xFA, 0xC7, 0x1F, 
    0x16, 0xED, 0xE6, 0x08, 0x47, 0xD3, 0xDC, 0x15, 0x1C, 0x08, 0xE9, 0x1B, 
    0x15, 0xF6, 0xF0, 0xE1, 0x1D, 0x05, 0xC2, 0x37, 0x2E, 0xE4, 0xFF, 0xF7, 
    0xFE, 0xEA, 0xF0, 0x0C, 0xE9, 0xD1, 0x10, 0x19, 0xE0, 0xD9, 0x08, 0x0E, 
    0xE7, 0xEC, 0xF1, 0xED, 0x07, 0x0B, 0xE8, 0x12, 0xEB, 0xF0, 0x07, 0xD9, 
    0x10, 0x08, 0xF4, 0x07, 0xEB, 0xE6, 0x22, 0x09, 0xE4, 0x13, 0xE8, 0xE3, 
    0xFC, 0x13, 0x0F, 0x03, 0xF9, 0x02, 0xEE, 0xDC, 0x00, 0x38, 0x04, 0xEF, 
    0x33, 0xE2, 0xD9, 0x02, 0x09, 0x10, 0xF1, 0xF7, 0x06, 0xF8, 0xFC, 0x26, 
    0xFF, 0x19, 0x02, 0xCB, 0xFF, 0x2E, 0x1C, 0xEA, 0xFE, 0x29, 0xE0, 0xE9, 
    0x33, 0xF4, 0xEC, 0x11, 0xD4, 0x0D, 0x15, 0xF3, 0xE1, 0xE6, 0x1E, 0x17, 
    0xF5, 0xEF, 0xFF, 0xE6, 0xFD, 0x12, 0xFF, 0xE0, 0xFD, 0x18, 0xEB, 0xF1, 
    0x0D, 0x26, 0xFD, 0xF7, 0xE8, 0x1B, 0x14, 0xE9, 0x32, 0x05, 0x0D, 0xF6, 
    0xE2, 0x26, 0x2F, 0xF6, 0xEE, 0xF5, 0xE2, 0x0A, 0x0F, 0xF7, 0x15, 0xF3, 
    0xB8, 0x22, 0x07, 0xEE, 0x22, 0xD8, 0xE1, 0xE9, 0x00, 0x21, 0xDE, 0xE7, 
    0x10, 0x08, 0xD9, 0xEB, 0x1E, 0x17, 0xD3, 0xEE, 0x27, 0x02, 0xFC, 0x02, 
    0x12, 0xEF, 0xD8, 0x06, 0x18, 0x1E, 0xF0, 0xEA, 0xF0, 0xFF, 0x1F, 0xF1, 
    0x10, 0x0A, 0xD2, 0xE9, 0x26, 0x1D, 0xE9, 0xC7, 0xF3, 0x0E, 0xFF, 0xFB, 
    0xF6, 0x19, 0x02, 0xCB, 0x1D, 0x26, 0xDC, 0xD3, 0xDB, 0x0F, 0xFB, 0xE9, 
    0x05, 0xD7, 0x02, 0xF1, 0xEC, 0x1F, 0xE2, 0xEA, 0xD4, 0x03, 0x09, 0xD7, 
    0x2F, 0xF0, 0xC7, 0x37, 0x0A, 0xF1, 0xF9, 0xF2, 0x1D, 0x01, 0xF2, 0x00, 
    0x2E, 0xF6, 0xEF, 0x03, 0xFD, 0x3B, 0xF1, 0xE8, 0x11, 0xFB, 0x0F, 0xFC, 
    0xEC, 0xFB, 0xF6, 0xF2, 0x24, 0xF4, 0xE2, 0xE8, 0xED, 0xFA, 0x0F, 0x04, 
    0xD7, 0xE9, 0xEF, 0x27, 0x02, 0xC5, 0xFE, 0x14, 0xF3, 0xEC, 0xF2, 0x01, 
    0x2B, 0xD7, 0x04, 0x47, 0xE5, 0xEE, 0xE5, 0xFC, 0x02, 0x09, 0xE1, 0xFB, 
    0x1B, 0xFE, 0xFA, 0x0B, 0x15, 0xEA, 0x08, 0x03, 0xDF, 0x1B, 0x24, 0xE1, 
    0xFC, 0x04, 0xFC, 0x1E, 0xFF, 0xE3, 0xFD, 0x1D, 0x08, 0xED, 0x06, 0x28, 
    0xE3, 0xFE, 0x17, 0xD4, 0x25, 0xE6, 0xBD, 0x2C, 0x35, 0xCB, 0xF8, 0xF0, 
    0xCB, 0x23, 0x2C, 0xE2, 0xD9, 0x23, 0xF6, 0xFE, 0xF6, 0xFD, 0x03, 0xF3, 
    0x16, 0xED, 0x35, 0x08, 0xBF, 0xFE, 0x26, 0x1F, 0xE2, 0xDA, 0xC4, 0x1C, 
    0x25, 0xCB, 0xD4, 0x0D, 0x23, 0xD9, 0xF4, 0x0C, 0xE7, 0x09, 0x1B, 0xC7, 
    0x19, 0x0F, 0xB8, 0x2C, 0xFF, 0xC6, 0x09, 0x00, 0xF8, 0xE2, 0x11, 0x0B, 
    0x05, 0x0A, 0xEA, 0xFF, 0xFB, 0x0D, 0x09, 0x01, 0xEF, 0x09, 0xF0, 0x06, 
    0x31, 0xD5, 0xF0, 0x19, 0xD6, 0x15, 0x1D, 0xBD, 0xD8, 0x21, 0x06, 0xFE, 
    0xF7, 0xE2, 0xF5, 0x11, 0x0C, 0xEB, 0x00, 0x04, 0xEC, 0xDC, 0x01, 0x1B, 
    0xFA, 0xDD, 0xFF, 0x16, 0xF9, 0xFD, 0xE5, 0xE8, 0x13, 0xE8, 0x04, 0x27, 
    0xE2, 0xFD, 0x16, 0x0E, 0xFA, 0xF4, 0x04, 0x14, 0xFD, 0xF0, 0x12, 0xE9, 
    0xE3, 0x11, 0x0B, 0x10, 0xF7, 0xD7, 0x16, 0x1F, 0x06, 0x0E, 0xEC, 0xFE, 
    0x25, 0x20, 0xED, 0x02, 0xEF, 0xEA, 0x03, 0x11, 0xF3, 0xE0, 0xF9, 0x02, 
    0xFF, 0xE3, 0x0D, 0x07, 0xD9, 0xF8, 0x14, 0x0F, 0xE5, 0xF6, 0xF1, 0xFF, 
    0x1C, 0x02, 0xF0, 0xE7, 0x06, 0xFA, 0xFD, 0x1D, 0xEF, 0xD0, 0x0D, 0x15, 
    0xE1, 0x03, 0xF1, 0xF6, 0x38, 0xE7, 0xDE, 0x07, 0xD3, 0x1E, 0x15, 0xDB, 
    0xF2, 0xDD, 0x3A, 0x31, 0xE7, 0xDE, 0xD6, 0x0A, 0x1E, 0x0C, 0xE5, 0xDF, 
    0x00, 0x1D, 0x0A, 0xEB, 0xDE, 0xEA, 0x10, 0x1F, 0xF6, 0xDA, 0xF3, 0x01, 
    0x05, 0xEA, 0x0A, 0xF5, 0xD2, 0xFC, 0x18, 0xEA, 0xF0, 0x17, 0xF4, 0xEF, 
    0xF3, 0x0E, 0x18, 0xE9, 0x0B, 0xF9, 0x09, 0x2D, 0xE8, 0x15, 0xFC, 0x12, 
    0x27, 0xE3, 0x10, 0xD7, 0x1A, 0x47, 0x0D, 0xF7, 0xE3, 0x0E, 0x2A, 0x1A, 
    0xF1, 0xEB, 0x05, 0x13, 0x30, 0x06, 0xC9, 0xE1, 0x06, 0x36, 0xFE, 0xE8, 
    0xEF, 0xF5, 0x11, 0xF7, 0xEA, 0xDD, 0x0F, 0x15, 0xF7, 0xDB, 0xEA, 0x01, 
    0xF4, 0x17, 0xF6, 0xD9, 0xF4, 0x13, 0x06, 0xEC, 0xF6, 0xE6, 0x11, 0x0B, 
    0xD9, 0xED, 0xF3, 0x1A, 0xED, 0xE7, 0x02, 0xF3, 0x0A, 0x0F, 0xFB, 0xF1, 
    0xE1, 0x01, 0x16, 0x03, 0xEA, 0xDB, 0x06, 0x18, 0xFC, 0xF7, 0xFC, 0xEF, 
    0x11, 0x1F, 0xE9, 0x0E, 0xFA, 0x00, 0x27, 0x03, 0xDA, 0xDF, 0xF9, 0x35, 
    0x0C, 0xC9, 0x08, 0x00, 0x34, 0x12, 0xE7, 0xF8, 0xE9, 0x09, 0x0D, 0x02, 
    0xE2, 0xE7, 0x11, 0x21, 0xE9, 0xD3, 0xF5, 0x07, 0x18, 0xEA, 0xE4, 0xF5, 
    0x19, 0x1D, 0xEF, 0xE9, 0xE3, 0x07, 0x1E, 0xF0, 0xD1, 0xED, 0x11, 0x15, 
    0xF8, 0xDD, 0xF5, 0xEC, 0x11, 0x20, 0xE4, 0xED, 0xD6, 0xEA, 0x3A, 0xFB, 
    0xE3, 0xDB, 0xFD, 0x28, 0xF6, 0xFD, 0xDE, 0xFA, 0x28, 0xFD, 0xEC, 0xF1, 
    0xFF, 0x14, 0x18, 0xF1, 0xEC, 0x0D, 0xF8, 0x12, 0x24, 0xFB, 0xF5, 0xF0, 
    0x1A, 0x2A, 0xE8, 0xDF, 0x08, 0x10, 0x09, 0x03, 0xE7, 0x00, 0x0E, 0x12, 
    0xF8, 0xFB, 0x17, 0xED, 0xFE, 0x0D, 0xF6, 0xF2, 0xFE, 0x24, 0x0A, 0xF3, 
    0xEF, 0x02, 0x14, 0xF1, 0xEC, 0x01, 0x0D, 0x09, 0x06, 0xFD, 0xE1, 0xE5, 
    0xFD, 0x13, 0x16, 0xEF, 0xD3, 0x01, 0x13, 0x0D, 0xE0, 0xE6, 0x02, 0x11, 
    0x03, 0xF7, 0xEB, 0xF5, 0x05, 0xFD, 0x0F, 0xFA, 0xEB, 0xEE, 0x11, 0x0C, 
    0xEC, 0xF0, 0xFC, 0x14, 0x04, 0xF6, 0xF3, 0x04, 0x06, 0x04, 0x02, 0xEA, 
    0xFB, 0x11, 0x1A, 0x08, 0xEE, 0xF1, 0x01, 0x0F, 0x12, 0xF3, 0xE6, 0x09, 
    0x17, 0xF9, 0xEE, 0x06, 0x16, 0x07, 0xFA, 0xF8, 0x0B, 0x04, 0xF8, 0x18, 
    0x01, 0xEC, 0xF7, 0x02, 0x18, 0x04, 0xEC, 0xF0, 0x15, 0x06, 0x01, 0xFD, 
    0xEA, 0x10, 0x01, 0x0F, 0xFA, 0xEE, 0x0D, 0x12, 0x15, 0xE1, 0xE9, 0x0B, 
    0x11, 0x00, 0xFB, 0xED, 0x02, 0x0D, 0xF5, 0xF8, 0xFB, 0xFE, 0xFB, 0x06, 
    0x08, 0xED, 0xF1, 0x0A, 0x13, 0xFF, 0xE7, 0xF7, 0x0B, 0x09, 0xF8, 0xFF, 
    0x04, 0x03, 0x01, 0x02, 0xF9, 0xF8, 0xFB, 0x0A, 0x15, 0xF8, 0xFC, 0xF0, 
    0x10, 0x1C, 0xEB, 0xFF, 0x05, 0x16, 0xF2, 0xF7, 0x14, 0xFA, 0x04, 0x0D, 
    0xFF, 0xED, 0xF9, 0x03, 0x0D, 0x01, 0xE9, 0xF3, 0x0F, 0x04, 0xF3, 0xF6, 
    0xF9, 0x00, 0x07, 0xF4, 0xF7, 0xF4, 0xFB, 0x18, 0x0C, 0xF1, 0xF4, 0xFE, 
    0x18, 0x0D, 0xF7, 0xEF, 0x01, 0x20, 0xFA, 0xEB, 0x0B, 0x07, 0x03, 0x06, 
    0xFD, 0xFA, 0xFD, 0x08, 0x18, 0xFA, 0xEE, 0xF9, 0x03, 0x20, 0xFD, 0xF0, 
    0xF4, 0x07, 0x18, 0x0F, 0xF5, 0xEB, 0x07, 0x22, 0x10, 0xF4, 0xF8, 0x02, 
    0x0C, 0x0E, 0x0C, 0xF2, 0xEE, 0xFE, 0x07, 0x09, 0xEF, 0xF3, 0xF6, 0x04, 
    0x07, 0xF1, 0xF4, 0xE7, 0x06, 0x0A, 0xFF, 0xED, 0xF1, 0xF4, 0xFB, 0x11, 
    0xF7, 0xF3, 0xF7, 0xFF, 0x07, 0xF6, 0xFD, 0x03, 0xFA, 0x08, 0x06, 0xF3, 
    0x00, 0xFD, 0x00, 0x06, 0x04, 0xEF, 0xF2, 0x09, 0x06, 0x09, 0xF1, 0xFB, 
    0x03, 0x0B, 0x09, 0xED, 0x00, 0xFC, 0x00, 0xFD, 0xFF, 0xF3, 0xF6, 0xFF, 
    0x01, 0x03, 0xED, 0xF6, 0xF3, 0x00, 0x03, 0xF4, 0xF2, 0xF7, 0x0F, 0xFE, 
    0xFB, 0xF8, 0xF5, 0x01, 0x0C, 0x0A, 0xF3, 0xF6, 0xFF, 0x16, 0x06, 0xF2, 
    0xF5, 0x0A, 0x0D, 0x0A, 0xFF, 0xF8, 0x02, 0x00, 0x05, 0x06, 0xF8, 0xF2, 
    0x05, 0x0D, 0x06, 0xF3, 0xF0, 0x01, 0x0C, 0x0A, 0xF6, 0xF3, 0xF6, 0x00, 
    0x06, 0xEF, 0xF2, 0x06, 0xFF, 0xF3, 0x0A, 0xFB, 0xEC, 0xFF, 0x06, 0x0C, 
    0xF7, 0xF4, 0x06, 0xFA, 0x05, 0x07, 0xFA, 0x03, 0xFB, 0x0A, 0x00, 0xF4, 
    0x0C, 0xFB, 0xFE, 0x12, 0x00, 0xEF, 0x07, 0x10, 0x08, 0x05, 0x03, 0xFC, 
    0xFE, 0x15, 0x05, 0xF7, 0xFF, 0x06, 0x08, 0x0E, 0xF7, 0xF4, 0x08, 0x0A, 
    0x03, 0xFD, 0xFB, 0xFD, 0x0B, 0x05, 0x00, 0xF5, 0xFF, 0x0E, 0x00, 0x02, 
    0xF3, 0xFD, 0x0D, 0x02, 0x00, 0xF7, 0xFF, 0xFE, 0x02, 0x0B, 0xFA, 0xF3, 
    0x02, 0x04, 0x02, 0x00, 0xED, 0x00, 0x07, 0x00, 0xFE, 0xF2, 0xFA, 0x07, 
    0xFE, 0xF6, 0x00, 0xFC, 0xF6, 0x07, 0x05, 0xFA, 0xF8, 0xF6, 0x0B, 0x03, 
    0xF5, 0xFD, 0xFA, 0x08, 0xFF, 0xFA, 0xF9, 0x00, 0x06, 0x00, 0xFD, 0xFC, 
    0x02, 0x00, 0x05, 0x05, 0xFD, 0xFB, 0x01, 0x06, 0x07, 0xFA, 0xFC, 0x08, 
    0x06, 0x02, 0xFC, 0xFB, 0x04, 0x0A, 0x07, 0xFE, 0xF8, 0xFF, 0x08, 0x0A, 
    0x02, 0xFD, 0xF8, 0x01, 0x0F, 0x06, 0xF7, 0xFD, 0x06, 0x08, 0x09, 0xFA, 
    0xF1, 0x01, 0x0B, 0x06, 0xF9, 0xF4, 0xFF, 0x08, 0x02, 0xFF, 0xF1, 0xFA, 
    0x0C, 0x05, 0xFF, 0xF1, 0xFA, 0x05, 0x07, 0x05, 0xF3, 0xF6, 0x01, 0x04, 
    0x02, 0xF9, 0xF4, 0x01, 0x0A, 0x05, 0xFB, 0xF4, 0xFF, 0x04, 0x05, 0x04, 
    0xF8, 0xFC, 0x04, 0x0A, 0x04, 0xF8, 0xFA, 0x04, 0x08, 0xFD, 0xF9, 0xFD, 
    0x00, 0x08, 0x01, 0xFB, 0xFC, 0xFF, 0x04, 0x02, 0xFF, 0xFB, 0xFD, 0x02, 
    0x05, 0xFE, 0xFC, 0x03, 0x00, 0x03, 0xFF, 0xFB, 0x01, 0x00, 0x06, 0xFD, 
    0xF9, 0x01, 0x04, 0x05, 0xFF, 0xFD, 0xFD, 0x02, 0x03, 0x00, 0xFB, 0xFE, 
    0x04, 0x03, 0x02, 0xFC, 0xF9, 0x00, 0x05, 0x04, 0xFF, 0xF6, 0x00, 0x04, 
    0x03, 0x00, 0xFB, 0xFF, 0x00, 0x04, 0x00, 0xFA, 0xFC, 0x04, 0x03, 0xFC, 
    0xFF, 0xFE, 0x02, 0x03, 0xFF, 0xFC, 0xFF, 0x04, 0xFF, 0xFF, 0xFF, 0xFF, 
    0x00, 0x03, 0x03, 0xFE, 0xFE, 0x02, 0x01, 0x01, 0xFE, 0xFD, 0x00, 0x04, 
    0x02, 0xFC, 0xFE, 0x02, 0x03, 0x00, 0xFC, 0xFE, 0xFF, 0x03, 0x00, 0xFE, 
    0xFE, 0xFE, 0xFF, 0x00, 0x04, 0xFD, 0xF9, 0x02, 0x03, 0x00, 0xFD, 0xFC, 
    0x00, 0x02, 0x00, 0xFF, 0xFA, 0xFE, 0x03, 0x00, 0x00, 0xFE, 0xFE, 0x01, 
    0x01, 0x01, 0xFF, 0xFC, 0x00, 0x02, 0x02, 0xFE, 0xFD, 0x02, 0x00, 0x01, 
    0xFF, 0xFE, 0x01, 0xFF, 0x01, 0x01, 0xFE, 0xFF, 0x00, 0x01, 0xFF, 0xFF, 
    0xFF, 0x00, 0x02, 0xFF, 0xFE, 0xFF, 0x02, 0x01, 0xFF, 0xFF, 0xFF, 0x00, 
    0x01, 0x00, 0xFE, 0xFF, 0x00, 0x00, 0x01, 0xFE, 0xFE, 0x00, 0x00, 0x00, 
    0x00, 0xFF, 0xFF, 0x00, 0x01, 0x00, 0xFF, 0xFE, 0x00, 0x01, 0x00, 0xFF, 
    0xFF, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0x01, 0x00, 0xFF, 
    0xFF, 0x00, 0x01, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 
    0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 
    0x00, 0xFF, 0x00, 0x00, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 
    0xFF, 0x00, 0x00, 0xFF, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 
    0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 
    0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0x00, 
    0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 
    0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0xFF, 
    0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 
    0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 
    0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0x00, 
    0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0xF8, 0x47, 
    0x09, 0xE0, 0x31, 0x04, 0xEC, 0xF3, 0x23, 0x04
};

